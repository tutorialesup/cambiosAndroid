package com.unprogramador.shingrey.cambiosandroid;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Edittexts extends AppCompatActivity {
    EditText et3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edittexts);
        et3 = findViewById(R.id.editText3);
        et3.setError(getString(R.string.app_name), getDrawable(R.drawable.ic_launcher_foreground));
        et3.setBackgroundTintList(ColorStateList.valueOf(Color.GREEN));
        Button regresar = findViewById(R.id.buttonregresar);
        regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
