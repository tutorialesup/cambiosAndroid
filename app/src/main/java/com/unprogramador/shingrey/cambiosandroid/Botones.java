package com.unprogramador.shingrey.cambiosandroid;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Botones extends AppCompatActivity {
    private int color = 0;
    private Button cambia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_botones);
        Button rcode = findViewById(R.id.button3);
        rcode.setBackgroundColor(Color.parseColor("#FF0000"));
        cambia = findViewById(R.id.button4);
        cambia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (color){
                    case 0:
                        cambia.setBackgroundColor(Color.parseColor("#4286f4"));
                        mensaje("#4286f4");
                        color++;
                        break;
                    case 1:
                        cambia.setBackgroundColor(Color.parseColor("#0f9e3c"));
                        mensaje("#0f9e3c");
                        color++;
                        break;

                    case 2:
                        cambia.setBackgroundColor(Color.parseColor("#e88e2e"));
                        mensaje("#e88e2e");
                        color++;
                        break;
                    case 3:
                        cambia.setBackgroundColor(Color.parseColor("#e82ea0"));
                        mensaje("#e82ea0");
                        color=0;
                        break;
                }
            }
        });
        Button atras = findViewById(R.id.button5);
        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
    public void mensaje(String msn){
        Toast.makeText(getApplicationContext(), msn, Toast.LENGTH_SHORT).show();

    }

}
