package com.unprogramador.shingrey.cambiosandroid;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button irBoton = findViewById(R.id.botonesmain);
        irBoton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent botones = new Intent(getApplication(),Botones.class);
                startActivity(botones);

            }
        });
        Button irInput = findViewById(R.id.inputmain);
        irInput.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent inputs = new Intent(getApplication(),Edittexts.class);
                startActivity(inputs);
            }
        });

    }
}
